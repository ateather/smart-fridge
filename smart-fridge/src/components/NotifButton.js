import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';

/* Props:

    label : string : a short message for the button
    message : string : a short written message or sub element content
    value : int : an accessable and comparable value
    comparator : (value) => {return bool} : a custom comparator to use do update hasMessage, default checks (value != 0)
    onClick : (value, update) => {} : an event fired on button click
        - update : (value, message) => {return comparator(value)} : if called, updates internal values


    ----- Semi-Private ----

    as : {} : Proxy object
    to : string : Proxy object property
    //objprops : {} : bootstrap.button extra props like `block`
    variant : string : bootstrap.button.variant string
    size : string : bootstrap.button.size string
    style : {} : general css 

*/
class NotificationButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value : this.props.value || 0,
            message: this.props.message || ""
        }
    }

    _checkValue() {
        let defaultComparator = (value) => {
            return (value !== 0)
        }
        let comparator = this.props.comparator || defaultComparator

        return comparator(this.state.value)
    }

    _onClick() {
        if (typeof this.props.onClick === "function") {
            this.props.onClick.call(this, this.props.value, (value, message) => {
                this.setState({
                    value : value,
                    message : message
                })
                
                return this.checkValue()
            })
        }
    }

    render() {
        let _message = <span></span>
        let displayMessage = this._checkValue()
        if (displayMessage) {
            _message = (
                <span style={{"fontSize" : "14px"}} className="badge badge-warning">{this.props.message}</span>
            )
        } else {
            _message = (<span className="badge"> </span>)
        }

        return (
            <div>
                <Row>
                    <Col>
                        {_message}
                    </Col>
                    
                </Row>
                
                <Row>
                    <Col>
                        <Button as={this.props.as} 
                                style={this.props.style}
                                to={this.props.to} 
                                variant={this.props.variant} 
                                size={this.props.size}
                                block
                                onClick={this._onClick.bind(this)}>
                            {this.props.label}
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default NotificationButton;