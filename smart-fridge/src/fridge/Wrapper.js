import React, { Component } from 'react';
import { Container, Button} from 'react-bootstrap';
import { Route } from "react-router-dom";
import Navbar from './Navbar'
import '../App.css';

import HomeScreen from "./HomeScreen.js"
import Inventory from "./Inventory.js"
import Nutrition from "./Nutrition.js"
import Recipes from "./Recipes.js"
import ShoppingList from "./ShoppingList.js"

const footerHeight = 60

const FOOTER = {
  position: 'fixed',
  bottom: 0,
  height: footerHeight + 'px', /* Set the fixed height of the footer here */
  lineHeight: footerHeight + 'px', /* Vertically center the text there */
  backgroundColor: '#f5f5f5'
}

const noteStyle = {
  position: 'absolute',
  bottom: footerHeight
}

const noteBox = {
  marginTop: '10px',
  width: '200px',
  color: 'white'
}

const gimic = {
  marginLeft: '10px',
  marginRight: '10px'
}

class Wrapper extends Component {
  constructor(props) {
    super(props)
    this.InputStorage = []
    this.state = {
      isExpanded : false,
      notesDisplayed : false
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  toggleFooter() {
    let state = !this.state.isExpanded
    this.setState({isExpanded : state})
  }

  toggleNotes() {
    let state = !this.state.notesDisplayed
    this.setState({notesDisplayed : state})
  }

  render() {
    let footer_button_text = (this.state.isExpanded) ? "<<" : "Apps >>"
    let notes_button_text = (this.state.notesDisplayed) ? "Hide Notepad" : "Notepad"

    let weather = <Button style={gimic} disabled variant="outline-seconday">Weather (73F Sunny)</Button>
    let stickyNoteToggle = <Button style={gimic} variant="outline-secondary" onClick={this.toggleNotes.bind(this)}> {notes_button_text} </Button>
    let notes = <footer className="navbar-dark bg-primary"  style={noteStyle}>
      <Container>
        <textarea className="navbar-dark bg-primary" style={noteBox} rows="10" placeholder="Write a message..." onChange={(e) => {this.handleChange.call(this, e, "Notes")}} value={this.InputStorage["Notes"]}></textarea>
      </Container>
    </footer>
    
    return (
        <div>
          <Navbar/>
          <Container className="App" style={{'marginBottom' : '60px', 'marginTop' : '90px'}}>

            <Route exact path="/fridge" component={HomeScreen} />
            <Route exact path="/fridge/inventory" component={Inventory} />
            <Route exact path="/fridge/nutrition" component={Nutrition} />
            <Route exact path="/fridge/recipes" component={Recipes} />
            <Route exact path="/fridge/list" component={ShoppingList} />

          </Container>

          {(this.state.notesDisplayed && this.state.isExpanded) ? notes : ""}
          <footer className="navbar-dark bg-primary" style={FOOTER}>
            <Container>
              {(this.state.isExpanded) ? weather : ""}
              {(this.state.isExpanded) ? stickyNoteToggle : ""}
              {(this.state.isExpanded) ? <Button style={gimic} disabled variant="outline-seconday">Download Apps</Button> : ""}
              <Button variant="outline-secondary" onClick={this.toggleFooter.bind(this)}> {footer_button_text} </Button>
            </Container>
          </footer>

        </div>
    );
  }
}

export default Wrapper;
