import React, { Component } from 'react';
import '../App.css';
import '../bootstrap.css';
import img1 from "../res/ButterChicken.jpg";
import { Container, Button } from 'react-bootstrap';
import ShoppingList from './ShoppingList'

class ButterChicken extends Component {
    constructor(props) {
        super(props);
    }

    addRecipe(item, quant, unit) {
        ShoppingList.addItem(item, quant, unit)
    }
    
    render() {
        return (
            <div>
                <title>Butter Chicken</title>
                <Button onClick={()=> this.addRecipe('item', 2, 'unit')} style={{position:'absolute', left:'310px', height: '45px'}} variant="outline-light" ><b>+ Add Recipe to Shopping List</b></Button>
                <Container style={{paddingLeft:'750px', position: "absolute", zIndex:'0'}}>
                    <img  src={img1}/>
                </Container>                    
                <body style={{paddingLeft:'100px'}}>
                    <header>
                        <section>
                            <h2>Butter Chicken</h2>
                            <p>The last butter chicken recipe you'll ever need!</p>
                        </section>
                        <section>
                            <p>
                                Chicken Makhani (Butter Chicken) is a full flavored dish that complements the chicken <br/>
                                well. It can be made as mild or spicy as you wish by adjusting the cayenne. Serve with <br/>
                                basmati rice and naan bread!
                            </p>
                            <p>The recipe below is designed to act as a guide, and not something to be followed <br/>
                                prescriptively. Play with your food! Adjust amounts based on your tastes.
                            </p>
                        </section>
                    </header>
                    <article>
                        <section id="ingredients">
                            <h3>Ingredients</h3>
                            <p>Serves 2 people</p>
                            <ul>
                                <li>1 tablespoon peanut oil</li>
                                <li>1 shallot, finely chopped</li>
                                <li>1/4 white onion, finely chopped</li>
                                <li>2 tablespoons butter</li>
                                <li>2 tablespoons lemon juice</li>
                                <li>1 tablespoon ginger-garlic paste</li>
                                <li>1 teaspoon garam masala</li>
                                <li>1 teaspoon chili powder</li>
                                <li>1 teaspoon ground cumin</li>
                                <li>1 bay leaf</li>
                                <li>1/4 cup plain yogurt</li>
                                <li>1 cup half and half</li>
                                <li>1 cup tomato puree</li>
                                <li>Cayenne powder to taste</li>
                                <li>Salt to taste</li>
                                <li>1 pinch black pepper</li>
                                <li>1 pound chicken breast, cut into bite size pieces</li>
                                <li>1 tablespoon cornstarch</li>
                                <li>1/4 cup water</li>
                            </ul>
                        </section>
                        <section>
                            <h3>Preparation</h3>
                            <ul>
                                <li>
                                    Heat 1 tablespoon oil in a large saucepan over medium high heat. Saute shallot <br/>
                                    and onion until soft and translucent. Stir in butter, lemon juice, ginger-garlic <br/>
                                    paste, 1 teaspoon garam masala, chili powder, cumin and bay leaf. Cook, stirring, <br/>
                                    for 1 minute. Add tomato sauce, and cook for 2 minutes, stirring frequently. Stir <br/>
                                    in half-and-half and yogurt. Reduce heat to low, and simmer for 10 minutes, stirring <br/>
                                    frequently. Season with salt pepper. Remove from heat and set aside. <br/>
                                </li>
                                <li>
                                    Heat 1 tablespoon oil in a large heavy skillet over medium heat. Cook chicken until <br/>
                                    lightly browned, about 10 minutes. Reduce heat, and season with 1 teaspoon garam <br/>
                                    masala and cayenne. Stir in a few spoonfuls of sauce, and simmer until liquid has <br/>
                                    reduced, and chicken is no longer pink. Stir cooked chicken into sauce.</li>
                                <li>
                                    Mix together cornstarch and water, then stir into the sauce. Cook for 5 to 10 minutes, <br/>
                                    or until thickened.
                                </li>
                                <li>Serve!</li>
                            </ul>
                        </section>
                    </article>
                </body>
            </div>
        );
    }
}

export default ButterChicken;
