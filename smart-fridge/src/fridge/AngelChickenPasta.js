import React, { Component } from 'react';
import '../App.css';
import '../bootstrap.css';
import img1 from "../res/AngelChickenPasta.jpg";
import { Container, Button } from 'react-bootstrap';
import ShoppingList from './ShoppingList'

class AngelChickenPasta extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <title>Angel Chicken Pasta</title>
                <Button style={{position:'absolute', left:'390px', height: '45px'}} variant="outline-light" ><b>+ Add Recipe to Shopping List</b></Button>
                <Container style={{paddingLeft:'750px', position: "absolute"}}>
                    <img  src={img1}/>
                </Container>                    
                <body style={{paddingLeft:'100px'}}>
                    <header>
                        <section>
                            <h2>Angel Chicken Pasta</h2>
                        </section>
                        <section>
                            <p>
                                A delicious, easy company dish - the flavors are wonderful. A great family dinner!
                            </p>
                            <p>The recipe below is designed to act as a guide, and not something to be followed <br/>
                                prescriptively. Play with your food! Adjust amounts based on your tastes.
                            </p>
                        </section>
                    </header>
                    <article>
                        <section id="ingredients">
                            <h3>Ingredients</h3>
                            <p>Serves 2 people</p>
                            <ul>
                                <li>6 skinless, boneless chicken breast halves</li>
                                <li>1/4 cup butter</li>
                                <li>1/2 cup white wine</li>
                                <li>1 package dry Italian-style salad dressing mix</li>
                                <li>1 can condensed golden mushroom soup</li>
                                <li>4 oz cream cheese with chives</li>
                                <li>1 lb angel hair pasta</li>
                            </ul>
                        </section>
                        <section>
                            <h3>Preparation</h3>
                            <ul>
                                <li>
                                    Preheat oven to 325 degrees F (165 degrees C).
                                </li>
                                <li>
                                    In a large saucepan, melt butter over low heat. Stir in the package of dressing <br/>
                                    mix. Blend in wine and golden mushroom soup. Mix in cream cheese, and stir until <br/>
                                    smooth. Heat through, but do not boil. Arrange chicken breasts in a single layer <br/>
                                    in a 9x13 inch baking dish. Pour sauce over.
                                </li>
                                <li>
                                    Bake for 60 minutes in the preheated oven. Twenty minutes before the chicken is <br/>
                                    done, bring a large pot of lightly salted water to a rolling boil. Cook pasta <br/>
                                    until al dente, about 5 minutes. Drain. Serve chicken and sauce over pasta.
                                </li>
                                <li>Serve!</li>
                            </ul>
                        </section>
                    </article>
                </body>
            </div>
        );
    }
}

export default AngelChickenPasta;
