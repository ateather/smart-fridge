import React from 'react';
import { Modal, Button, Card } from 'react-bootstrap';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Users from "../constants/users.json";


class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    let cUser = {}

    for (let key in Users) {
      let user = Users[key]
      if (user.isCurrent)
        cUser = user
    }

    this.state = {
      show: false,
      currentUser: cUser,
      time: new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric'})
    };
  }

  componentDidMount() {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }
  tick() {
    this.setState({
      time: new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric'})
    });
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  changeUser(name) {
    let cUser = {}

    for (let key in Users) {
      let user = Users[key]
      if (user.name == name) {
        cUser = user
        user.isCurrent = true
      } else {
        user.isCurrent = false
      }
        
    }

    this.setState({currentUser : cUser})
  }

  render() {
    let ModalBody = []

    for (let key in Users) {
      let user = Users[key]

      ModalBody.push(
        <>
          <Button variant={(user.isCurrent) ? "outline-success" : "outline-secondary"} block onClick={this.changeUser.bind(this, user.name)}>{user.name}</Button>
        </>
      )

    }

    var cur = window.location.pathname;
    //alert(currentLocation)

    let active = {"backgroundColor" : "gray"}
    let inactive = {}
    return (
      <div>
        <nav className="navbar fixed-top navbar-expand-md custom-navbar navbar-dark bg-primary">
            <button className="navbar-toggler navbar-toggler-right custom-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            </button>
            <Link style={(cur == "/fridge" ) ? active : inactive} className="navbar-brand" to="/fridge"><b>Home</b></Link>
            <Link style={(cur == "/fridge/inventory" ) ? active : inactive} className="navbar-brand" to="/fridge/inventory"><b>Inventory</b></Link>
            <Link style={(cur == "/fridge/recipes" ) ? active : inactive} className="navbar-brand" to="/fridge/recipes"><b>Recipes</b></Link>
            <Link style={(cur == "/fridge/list" ) ? active : inactive} className="navbar-brand" to="/fridge/list"><b>Shopping List</b></Link>
            <Link style={(cur == "/fridge/nutrition" ) ? active : inactive} className="navbar-brand" to="/fridge/nutrition"><b>Nutrition</b></Link>
            <div style={{paddingLeft:'80px', 'fontSize':'25px'}} className="centered">{this.state.time} - 5/14/2019</div>
            <div className="collapse navbar-collapse " id="collapsibleNavbar">
              <ul className="navbar-nav ml-auto ">
                <li className="nav-item">
                  <Button className="nav-link" style={{"fontSize" : "20px", color : "white", 'cursor' : 'pointer'}} onClick={this.handleShow}>{this.state.currentUser.name}</Button>
                </li>
              </ul>
            </div>
          </nav>


          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Select Current User</Modal.Title>
            </Modal.Header>
            <Modal.Body>{ModalBody}</Modal.Body>
            <Modal.Footer>
              <Button variant="outline-secondary" disabled>
                Add New User
              </Button>
              <Button variant="primary" onClick={this.handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
      </div>
    );
  }
}

export default Navbar;
