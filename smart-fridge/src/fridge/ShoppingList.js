import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import '../App.css';
import shoppingList from "../constants/shoppinglist.json"

const FSIZE = "40px"

class ShoppingList extends Component {

  constructor(props) {
    super(props)
    this.InputStorage = []
    this.state = {
      "shoppingList" : shoppingList,
      "adding" : false
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  doneAdding() {
    let done = !this.state.adding
    this.setState({adding : done})
  }

  addItem = (item, quant, unit) => {
    let nShoppingList = this.state.shoppingList
    nShoppingList[item] = {
      name : item,
      count : quant,
      unit : unit,
      expired : false
    }

    this.setState({ shoppingList : nShoppingList })
  }

  addNewItem() {
    let AddCount = this.InputStorage["AddCount"] || 1
    let Unit = this.InputStorage["Unit"] || ""
    if (this.InputStorage["AddName"] && AddCount > 0) {
      let nShoppingList = this.state.shoppingList
      nShoppingList[this.InputStorage["AddName"]] = {
        name : this.InputStorage["AddName"],
        count : AddCount,
        unit : Unit,
        expired : false
      }

      this.setState({ shoppingList : nShoppingList })
    }
  }

  updateItem(name, newValue) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      nShoppingList[name].count = (newValue > 0) ? newValue : 0
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  removeItem(name) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      delete nShoppingList[name]
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  check(name) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      nShoppingList[name].checked = !nShoppingList[name].checked
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  render() {
    let sL = this.state.shoppingList
    let RenderMe = []

    RenderMe.push((<>
      <Row key="_what_">
        <Col xs>

        </Col>
        <Col xs={4} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
          <span className="badge">Item Name</span>
        </Col>
        <Col xs={3} style={{'text-align':'right', 'font-size':FSIZE}}>
          <span className="badge" style={{"marginRight" : "80px", "textAlign" : "right"}}>Count</span>
        </Col>
        <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
          <span className="badge">Unit</span>
        </Col>
      </Row>
      <br/>
      </>
    ))

    for (let key in sL) {
      let i = sL[key]
      if (i.checked) {
        RenderMe.push((
          <Row key={i.name} style={{'font-size':FSIZE}}>
            <Col xs>
              <Button variant="outline-danger" onClick={this.removeItem.bind(this, i.name)}>x</Button>
              <Button active variant="outline-success" onClick={this.check.bind(this, i.name)}>&#10003;</Button>
            </Col>
            <Col xs={4} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
              <span>{i.name}</span>
            </Col>
            <Col xs={3} style={{'font-size':FSIZE}}>
              <Button variant="outline-secondary" onClick={this.updateItem.bind(this, i.name, i.count-1)}>-</Button>
              <span className={i.expired ? "badge" : "badge"} style={{"marginRight" : "0px", "textAlign" : "center"}}>{i.count}</span>
              <Button variant="outline-light" onClick={this.updateItem.bind(this, i.name, i.count+1)}>+</Button>
            </Col>
            <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
              <span>{i.unit}</span>
            </Col>
          </Row>
        ))
      } else {
        RenderMe.push((
          <Row key={i.name} style={{'font-size':FSIZE}}>
            <Col xs>
              <Button variant="outline-danger" onClick={this.removeItem.bind(this, i.name)}>x</Button>
              <Button variant="outline-success" onClick={this.check.bind(this, i.name)}>&#10003;</Button>
            </Col>
            <Col xs={4} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
              <span>{i.name}</span>
            </Col>
            <Col xs={3} style={{'font-size':FSIZE}}>
              <Button variant="outline-secondary" onClick={this.updateItem.bind(this, i.name, i.count-1)}>-</Button>
              <span className={i.expired ? "badge" : "badge"} style={{"marginRight" : "0px", "textAlign" : "center"}}>{i.count}</span>
              <Button variant="outline-light" onClick={this.updateItem.bind(this, i.name, i.count+1)}>+</Button>
            </Col>
            <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
              <span>{i.unit}</span>
            </Col>
          </Row>
        ))
      }
      /*RenderMe.push((
        <div style={{'border-bottom':'1px solid white'}}></div>
      ))*/
    }

    let header = (<h1>Shopping List</h1>)

    let footer = (<div></div>)
    if (!this.state.adding) {
      footer = (
        <Row>
          <Col>
            <Button style={{'font-size':FSIZE}} variant="outline-secondary" onClick={this.doneAdding.bind(this)}>Add Item</Button>
          </Col>
          <Col>
            <Button style={{'font-size':FSIZE}} variant="outline-secondary">Checked Items To Inventory</Button>
          </Col>
        </Row>
      )
    } else {
      footer = (
      <Row>
        <Col sm={6}>
          <input type="text" style={{"color" : "white", 'font-size':FSIZE}} className="form-control bg-primary" placeholder="Item's name" onChange={(e) => {this.handleChange.call(this, e, "AddName")}}/>
        </Col>

        <Col xs>
          <input type="text" style={{"textAlign" : "right", "color" : "white", 'font-size':FSIZE}} className="form-control bg-primary" pattern="[0-9]*" value={this.InputStorage["AddCount"] || 1} onChange={(e) => {this.handleChange.call(this, e, "AddCount")}}/>
        </Col>

        <Col xs={2}>
          <input type="text" style={{"textAlign" : "right", "color" : "white", 'font-size':FSIZE}} className="form-control bg-primary"  placeholder='Unit' value={this.InputStorage["Unit"] || ""} onChange={(e) => {this.handleChange.call(this, e, "Unit")}}/>
        </Col>

        <Col xs>
          <Button style={{'font-size':FSIZE}} variant="outline-light" id="button-addon2" block onClick={this.addNewItem.bind(this)} >Add</Button>
        </Col>
        <Col xs>
          <Button style={{'font-size':FSIZE}} variant="outline-secondary" id="button-addon2" block onClick={this.doneAdding.bind(this)}>Done</Button>
        </Col>


      </Row>
      )
    }

    return (
      <div>
        {header}
        <br/>
        {RenderMe}
        <br/>
        {footer}
        <br/>
      </div>
    );
  }


  // render() {
  //   return (
  //     <Container className="App">
  //       <hX style={{'font-size':'60px',}}>Shopping List</hX>
  //       <ul style={{'text-align':'top', 'font-size':'50px'}}>
  //        <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white', 'border-top':'1px solid white'}}>
  //           <div ><p>Eggs</p></div>
  //       </li>
  //        <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //           <div ><p>Milk</p></div>
  //       </li>
  //        <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //           <div ><p>Chicken</p></div>
  //       </li>
  //       <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //          <div ><p>Provolone</p></div>
  //      </li>
  //       <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //          <div ><p>Arnold Palmer</p></div>
  //      </li>
  //      <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //         <div ><p>Whipped Cream</p></div>
  //     </li>
  //      <li style={{'display':'block', 'text-align':'left', 'height':'80px', 'border-bottom':'1px solid white'}}>
  //         <div ><p>Yellow Mustard</p></div>
  //     </li>
  //      </ul>
  //     </Container>
  //   );
  // }
}

export default ShoppingList;
