import React, { Component } from 'react';
import '../App.css';
import '../bootstrap.css';
import img1 from "../res/ChickenCurry.jpg";
import { Container, Button } from 'react-bootstrap';

class ChickenCurry extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <title>Chicken Curry</title>
                <Button style={{position:'absolute', left:'390px', height: '45px'}} variant="outline-light" ><b>+ Add Recipe to Shopping List</b></Button>
                <Container style={{paddingLeft:'750px', position: "absolute"}}>
                    <img  src={img1}/>
                </Container>                    
                <body style={{paddingLeft:'100px'}}>
                    <header>
                        <section>
                            <h2>Chicken Curry</h2>
                        </section>
                        <section>
                            <p>
                                A delicious, easy company dish - the flavors are wonderful. A great family dinner!
                            </p>
                            <p>The recipe below is designed to act as a guide, and not something to be followed <br/>
                                prescriptively. Play with your food! Adjust amounts based on your tastes.
                            </p>
                        </section>
                    </header>
                    <article>
                        <section id="ingredients">
                            <h3>Ingredients</h3>
                            <p>Serves 2 people</p>
                            <ul>
                                <li>13-ounce can coconut milk</li>
                                <li>3 tablespoons coconut oil</li>
                                <li>1 pound boneless skinless chicken breast</li>
                                <li>3 tablespoons Thai red curry paste</li>
                                <li>1 tablespoon lime juice</li>
                                <li>1 teaspoon kosher salt</li>
                                <li>2 teaspoons ground coriander</li>
                            </ul>
                        </section>
                        <section>
                            <h3>Preparation</h3>
                            <ul>
                                <li>
                                    To a large skillet, add the oil, onion, and sauté over medium-high heat until the <br/>
                                    onion begins to soften about 5 minutes; stir intermittently.
                                </li>
                                <li>
                                    Add the chicken and cook for about 5 minutes, or until chicken is done; flip and <br/>
                                    stir often to ensure even cooking.
                                </li>
                                <li>
                                    Add the coconut milk, carrots, Thai curry paste, salt, pepper, and stir to combine. <br/>
                                    Reduce the heat to medium, and allow mixture to gently boil for about 5 minutes, or <br/>
                                    until liquid volume has reduced as much as desired and thickens slightly.
                                </li>
                                <li>Serve!</li>
                            </ul>
                        </section>
                    </article>
                </body>
            </div>
        );
    }
}

export default ChickenCurry;
