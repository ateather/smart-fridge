import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import '../App.css';
import inventory from "../constants/inventory.json"

const FSIZE = "40px"

class Recipes extends Component {

  constructor(props) {
    super(props)
    this.InputStorage = []
    this.state = {
      "inventory" : inventory,
      "adding" : false
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  doneAdding() {
    let done = !this.state.adding
    this.setState({adding : done})
  }

  addNewItem() {
    let AddCount = this.InputStorage["AddCount"] || 1
    if (this.InputStorage["AddName"] && AddCount > 0 && this.InputStorage['AddDate']) {
      let nInventory = this.state.inventory
      nInventory[this.InputStorage["AddName"]] = {
        name : this.InputStorage["AddName"],
        count : AddCount,
        expired : false,
        expiration : new Date(this.InputStorage['AddDate'])
      }

      /*this.InputStorage["AddName"] = null
      this.InputStorage["AddDate"] = null
      this.InputStorage["AddCount"] = null*/

      this.setState({ inventory : nInventory })
    }
  }

  updateItem(name, newValue) {
    let nInventory = this.state.inventory
    if (nInventory[name]) {
      nInventory[name].count = (newValue > 0) ? newValue : 0
    }

    this.setState(state => ({
      "inventory" : nInventory
    }))
  }

  removeItem(name) {
    let nInventory = this.state.inventory
    if (nInventory[name]) {
      delete nInventory[name]
    }

    this.setState(state => ({
      "inventory" : nInventory
    }))
  }

  render() {
    let inv = this.state.inventory
    let RenderMe = []



    RenderMe.push(<>
    <Row key="_xxx_" style={{'font-size':FSIZE}}>
      <Col xs>

      </Col>
      <Col xs={4} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
        <span className="badge">Item Name</span>
      </Col>
      <Col xs>
        <span className={"badge align-middle"} style={{"marginRight" : "20px", "textAlign" : "right"}}>Count</span>
      </Col>
      <Col xs>
        <span className={"badge align-middle"}>Unit</span>
      </Col>
      <Col xs>
        <span className={"badge align-middle"}>Expiration Date</span>
      </Col>
    </Row>
    <br/>
    </>)
    for (let key in inv) {
      let i = inv[key]
      i.expiration = new Date(i.expiration)
      i.expired = (i.expiration <= new Date(Date.now()))

      let dateOffset = (24*60*60*1000) * 4;
      let warningDate = new Date(i.expiration);
      warningDate.setTime(warningDate.getTime() - dateOffset);
      let isClose = (warningDate <= new Date(Date.now()) )




      RenderMe.push((
        <Row key={i.name} style={{'font-size':FSIZE}}>
          <Col xs>
            <Button variant="outline-danger" onClick={this.removeItem.bind(this, i.name)}>x</Button>
          </Col>
          <Col xs={3} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
            <span>{i.name}</span>
          </Col>
          <Col xs={3}>
            <Button variant="outline-secondary" onClick={this.updateItem.bind(this, i.name, i.count-1)}>-</Button>
            <span className={i.expired ? "badge" : "badge"} style={{"marginRight" : "0px", "textAlign" : "center"}}>{i.count}</span>
            <Button variant="outline-light" onClick={this.updateItem.bind(this, i.name, i.count+1)}>+</Button>
          </Col>
          <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
            <span>{i.unit}</span>
          </Col>
          <Col xs>
            <span className={i.expired ? "badge badge-danger" : (isClose) ? "badge badge-warning" : "badge"}>{(i.expiration.getMonth()+1) + "/" + i.expiration.getDate() + "/" + (i.expiration.getYear() -100)}</span>
          </Col>
        </Row>
      ))
    }

    let header = (<div></div>)
    if (!this.state.adding) {
      header = (
        <Row>
          <Col>
            <h1>Inventory</h1>
          </Col>
        </Row>
      )
    } else {

      header = (
        <Row>
          <Col>
            <h1>Inventory</h1>
          </Col>
        </Row>
      )
    }

    let footer = (<div></div>)
    if (!this.state.adding) {
      footer = (
        <Row>
          <Col>
            <Button style={{'font-size':FSIZE}} variant="outline-secondary" onClick={this.doneAdding.bind(this)}>Add Item</Button>
          </Col>
        </Row>
      )
    } else {
      footer = (
      <Row>
        <Col sm={6}>
          <input type="text" style={{"color" : "white", 'font-size':FSIZE}} className="form-control bg-primary" placeholder="Item's name" onChange={(e) => {this.handleChange.call(this, e, "AddName")}}/>
        </Col>

        <Col xs={1}>
          <input type="text" style={{"textAlign" : "right", "color" : "white", 'font-size':FSIZE}} className="form-control bg-primary" pattern="[0-9]*" value={this.InputStorage["AddCount"] || 1} onChange={(e) => {this.handleChange.call(this, e, "AddCount")}}/>
        </Col>

        <Col xs={2}>
          <input type="text" style={{"textAlign" : "right", "color" : "white", 'font-size':FSIZE}} className="form-control bg-primary"  placeholder='Unit' value={this.InputStorage["Unit"] || ""} onChange={(e) => {this.handleChange.call(this, e, "Unit")}}/>
        </Col>
        <Col xs={3}>
          <input type="text" style={{"color" : "white", 'font-size':FSIZE}} className="form-control bg-primary" placeholder="05/20/2019" pattern="[0-1][0-9]/[0-3][0-9]/[0-2][0-9][0-9][0-9]" onChange={(e) => {this.handleChange.call(this, e, "AddDate")}}/>
        </Col>
        <Col xs>
          <Button style={{'font-size':FSIZE}} variant="outline-light" id="button-addon2" block onClick={this.addNewItem.bind(this)} >Add</Button>
        </Col>
        <Col xs>
          <Button style={{'font-size':FSIZE}} variant="outline-secondary" id="button-addon2" block onClick={this.doneAdding.bind(this)}>Done</Button>
        </Col>


      </Row>
      )
    }

    return (
      <div>
        {header}
        <br/>
        {RenderMe}
        <br/>
        {footer}
        <br/>
      </div>
    );
  }
}

export default Recipes;
