import React, { Component } from 'react';
import { Row, Col, Button, Alert, Container } from 'react-bootstrap';
import { Link } from "react-router-dom";
import '../App.css';
import NotificationButton from "../components/NotifButton.js"
import inventory from "../constants/inventory.json"

const gimic = {
  marginLeft: '10px',
  marginRight: '10px'
}

const alertStyle = {
  "font-size" : "20px",
  marginTop: "15px"
}

class Homescreen extends Component {
  render() {
    let expItems = []
    let expiredItemCount = 0
    let alerts = []
    for (let key in inventory) {
      let i = inventory[key]
      i.expiration = new Date(i.expiration)
      i.expired = (i.expiration <= new Date(Date.now()))
      if (inventory[key].expired) {
        expiredItemCount++
        expItems.push(inventory[key])
        alerts.push(
        <Row>
          <Col>
            <span style={alertStyle} className="badge badge-warning">
              {inventory[key].name} just expired
            <Button style={gimic} className="justify-content-end" variant="primary">
              Remove Item
            </Button>
            <Button style={gimic} className="justify-content-end" variant="outline-light">
              Dismiss
            </Button>
            </span>
          </Col>
        </Row>
        )
      }
    }

    return (
        <div  style={{'marginTop' : '12%'}}>
          <Row>
              <Col>
                <NotificationButton label="Inventory"
                  style={{"fontSize" : "35px", 'height':'175px'}}
                  value={expiredItemCount}
                  message={(expiredItemCount) + " expired item"+ ((expiredItemCount > 1) ? "s" : "") +" detected!"}
                  as={Link} 
                  to="/fridge/inventory" 
                  variant="primary" 
                  size="lg" 
                  block
                />
                  
              </Col>
              <Col>
                  <Button style={{"fontSize" : "35px", 'height':'175px', "marginTop": "9%"}} as={Link} to="/fridge/recipes" variant="primary" size="lg" block>Recipes</Button>
              </Col>
              <Col>
                  <Button style={{"fontSize" : "35px", 'height':'175px', "marginTop": "9%"}} as={Link} to="/fridge/list" variant="primary" size="lg" block>Shopping List</Button>
              </Col>
              <Col>
                  <Button style={{"fontSize" : "35px", 'height':'175px', "marginTop": "9%"}} as={Link} to="/fridge/nutrition" variant="primary" size="lg" block>Nutrition</Button>
              </Col>
          </Row>
          <br/>
          {alerts}
        </div>
    );
  }
}

export default Homescreen;
