import React, { Component } from 'react';
import { Button, Container, ListGroup, OverlayTrigger, Popover, ProgressBar } from 'react-bootstrap';
import '../App.css';
import nutrition from "../constants/nutrition.json"

const nutritionBox = {
  width: '650px',
  height: '350px',
  border: '1px solid black',
  boxSizing: 'border-box'
};

const nutritionColumnLeft = {
  float: 'left',
  paddingRight: '10px',
  width: '25%'
};

const caloriesStyle = {
  textAlign: 'left',
  paddingLeft: '10px',
  lineHeight: '.1',
  fontSize: '20px'
}

const macroStyle = {
  textAlign: 'left',
  paddingLeft: '10px',
  lineHeight: '.1'
}



class Nutrition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "nutrition": nutrition,
      "multiplier": 1,
      "multiplierIndex": 3,
      "serving": "",
      "servingUnit": "",
      "currentCalories": 1200,
      "calorieGoal": 2200,
      "currentFat": 30,
      "fatGoal": 70,
      "currentSodium": 800,
      "sodiumGoal": 2300,
      "currentCholesterol": 105,
      "cholersterolGoal": 250,
      "item": "Select an item to view nutrition",
      "calories": 0,
      "fat": 0,
      "sodium": 0,
      "cholesterol": 0
    };
  }

  chooseFood = (i) => {
    let nutritionList = this.state.nutrition;
    this.setState({ "multiplier": 1 });
    this.setState({ "multiplierIndex": 3});
    this.setState({ "item": i });
    this.setState({ "serving": nutritionList[i].serving });
    this.setState({ "servingUnit": nutritionList[i].servingUnit });
    this.setState({ "calories": nutritionList[i].calories });
    this.setState({ "fat": nutritionList[i].fat });
    this.setState({ "sodium": nutritionList[i].sodium });
    this.setState({ "cholesterol": nutritionList[i].cholesterol });
  }

  getLabel = (value, total) => {
    if (value === 0)
      return "";
    if ((value / total) < .03)
      return "";
    return Math.round(value);
  }

  getLeftLabel = (current, item, total, unit) => {
    const left = (total - current - item);
    if (left <= 0)
      return "";
    if ((left / total) < .12)
      return "";
    return Math.round(left) + unit + " left";
  }

  getNowVariant = (before, current, total) => {
    if (before + current > total)
      return "danger";
    return "warning";
  }

  getNowItem = (before, current, total) => {
    if (before + current > total) {
      return ((total - before) / total * 100);
    }

    return (current / total * 100);
  }

  getNowLeft = (before, current, total) => {
    if (before + current >= total)
      return 0;
    return ((total - current - before) / total * 100);
  }

  createList = () => {
    let items = [];
    for (let key in this.state.nutrition) {
      let food = this.state.nutrition[key];
      items.push(<ListGroup.Item onClick={() => this.chooseFood(food.name)}>{food.name + " (" + food.count + (food.unit === "" ? "" : " ") + food.unit + ")"}</ListGroup.Item>);
    }
    return items;
  }

  getServing = (serving, servingUnit) => {
    if (serving === "" && servingUnit === "")
      return "";
    if (servingUnit === "")
      return " - " + (serving * this.state.multiplier);
    return " - " + (serving * this.state.multiplier) + " " + servingUnit;
  }

  increaseMultiplier = () => {
    let mult = this.state.multiplier;
    if (mult === 4)
      return;
    if (mult < 1) {
      this.setState({"multiplier": this.state.multiplier * 2});
      return;
    }
    this.setState({"multiplier": this.state.multiplier + 1});
  }

  decreaseMultiplier = () => {
    let mult = this.state.multiplier;
    if (mult === .25)
      return;
    if (mult <= 1) {
      this.setState({"multiplier": this.state.multiplier / 2});
      return;
    }
    this.setState({"multiplier": this.state.multiplier - 1});
  }

  getWarning = (food) => {
    if (food === "Select an item to view nutrition")
      return;
    let nutritionList = this.state.nutrition;
    if (!("warnings" in nutritionList[food]))
      return;
    return <OverlayTrigger trigger="click" placement="right" overlay={
      <Popover id="popover-basic" title="Warnings">
      {nutritionList[food].warnings}
    </Popover>
    }>
    <Button style={{ float: 'left', marginLeft: '10px' }} variant="outline-warning">!</Button>
    </OverlayTrigger>
  }

  eatFood = () => {
    this.setState({"currentCalories": this.state.currentCalories + (this.state.calories * this.state.multiplier)});
    this.setState({"currentFat": this.state.currentFat + (this.state.fat * this.state.multiplier)});
    this.setState({"currentSodium": this.state.currentSodium + (this.state.sodium * this.state.multiplier)});
    this.setState({"currentCholesterol": this.state.currentCholesterol + (this.state.cholesterol * this.state.multiplier)});
  }



  render() {
    return (
      <Container className="App">
        <div class="row">
          <column style={nutritionColumnLeft}>
            <ul class="Nutrition-List-Group">
              {this.createList()}
            </ul>
          </column>
          <div class="Nutrition-Column-Right">
            <div style={nutritionBox}>
              <br></br>
              <p style={{ fontSize: '30px' }}>{this.state.item + this.getServing(this.state.serving, this.state.servingUnit)}
                <Button onClick={() => this.eatFood()} style = {{ float: 'left', marginLeft: '10px'}}>Eat</Button>
                {this.getWarning(this.state.item)}
                <Button onClick={() => this.increaseMultiplier()} style={{ float: 'right', marginRight: '10px' }} variant="outline-light"> +</Button>
                <Button onClick={() => this.decreaseMultiplier()} style={{ float: 'right', marginLeft: '10px' }} variant="outline-light" >-</Button>
              </p>
              <br></br>
              <p style={caloriesStyle}>Calories
                <span style={{ fontWeight: 'bold', float: 'right', paddingRight: '10px', fontSize: '20px' }}>{this.state.calories * this.state.multiplier} - {Math.round((this.state.calories * this.state.multiplier) / this.state.calorieGoal * 100)}%</span>
              </p>
              <ProgressBar style={{ margin: '20px' }}>
                <ProgressBar style={{ color: 'white' }} variant="success" now={this.state.currentCalories / this.state.calorieGoal * 100} label={this.state.currentCalories} />
                <ProgressBar style={{ color: 'white' }} variant={this.getNowVariant(this.state.currentCalories, this.state.calories * this.state.multiplier, this.state.calorieGoal)} now={this.getNowItem(this.state.currentCalories, this.state.calories * this.state.multiplier, this.state.calorieGoal)} label={this.getLabel(this.state.calories * this.state.multiplier, this.state.calorieGoal)} />
                <ProgressBar style={{ color: 'black', backgroundColor: 'white' }} now={this.getNowLeft(this.state.currentCalories, this.state.calories * this.state.multiplier, this.state.calorieGoal)} label={this.getLeftLabel(this.state.currentCalories, this.state.calories * this.state.multiplier, this.state.calorieGoal, " calories")} />
              </ProgressBar>
              <p style={macroStyle}>Fat
                <span style={{ fontWeight: 'bold', float: 'right', paddingRight: '10px' }}>{Math.round(this.state.fat * this.state.multiplier)}g - {Math.round((this.state.fat * this.state.multiplier) / this.state.fatGoal * 100)}%</span>
              </p>
              <ProgressBar style={{ margin: '20px' }}>
                <ProgressBar style={{ color: 'white' }} variant="success" now={this.state.currentFat / this.state.fatGoal * 100} label={this.state.currentFat} />
                <ProgressBar style={{ color: 'white' }} variant={this.getNowVariant(this.state.currentFat, this.state.fat * this.state.multiplier, this.state.fatGoal)} now={this.getNowItem(this.state.currentFat, this.state.fat * this.state.multiplier, this.state.fatGoal)} label={this.getLabel(this.state.fat * this.state.multiplier, this.state.fatGoal)} />
                <ProgressBar style={{ color: 'black', backgroundColor: 'white' }} now={this.getNowLeft(this.state.currentFat, this.state.fat * this.state.multiplier, this.state.fatGoal)} label={this.getLeftLabel(this.state.currentFat, this.state.fat * this.state.multiplier, this.state.fatGoal, "g")} />
              </ProgressBar>
              <p style={macroStyle}>Sodium
                <span style={{ fontWeight: 'bold', float: 'right', paddingRight: '10px' }}>{this.state.sodium * this.state.multiplier}mg - {Math.round((this.state.sodium * this.state.multiplier) / this.state.sodiumGoal * 100)}%</span>
              </p>
              <ProgressBar style={{ margin: '20px' }}>
                <ProgressBar style={{ color: 'white' }} variant="success" now={this.state.currentSodium / this.state.sodiumGoal * 100} label={this.state.currentSodium} />
                <ProgressBar style={{ color: 'white' }} variant={this.getNowVariant(this.state.currentSodium, this.state.sodium * this.state.multiplier, this.state.sodiumGoal)} now={this.getNowItem(this.state.currentSodium, this.state.sodium * this.state.multiplier, this.state.sodiumGoal)} label={this.getLabel(this.state.sodium * this.state.multiplier, this.state.sodiumGoal)} />
                <ProgressBar style={{ color: 'black', backgroundColor: 'white' }} now={this.getNowLeft(this.state.currentSodium, this.state.sodium * this.state.multiplier, this.state.sodiumGoal)} label={this.getLeftLabel(this.state.currentSodium, this.state.sodium * this.state.multiplier, this.state.sodiumGoal, "mg")} />
              </ProgressBar>
              <p style={macroStyle}>Cholesterol
                <span style={{ fontWeight: 'bold', float: 'right', paddingRight: '10px' }}>{this.state.cholesterol * this.state.multiplier}mg - {Math.round((this.state.cholesterol * this.state.multiplier) / this.state.cholersterolGoal * 100)}%</span>
              </p>
              <ProgressBar style={{ margin: '20px' }}>
                <ProgressBar style={{ color: 'white' }} variant="success" now={this.state.currentCholesterol / this.state.cholersterolGoal * 100} label={this.state.currentCholesterol} />
                <ProgressBar style={{ color: 'white' }} variant={this.getNowVariant(this.state.currentCholesterol, this.state.cholesterol * this.state.multiplier, this.state.cholersterolGoal)} now={this.getNowItem(this.state.currentCholesterol, this.state.cholesterol * this.state.multiplier, this.state.cholersterolGoal)} label={this.getLabel(this.state.cholesterol * this.state.multiplier, this.state.cholersterolGoal)} />
                <ProgressBar style={{ color: 'black', backgroundColor: 'white' }} now={this.getNowLeft(this.state.currentCholesterol, this.state.cholesterol * this.state.multiplier, this.state.cholersterolGoal)} label={this.getLeftLabel(this.state.currentCholesterol, this.state.cholesterol * this.state.multiplier, this.state.cholersterolGoal, "mg")} />
              </ProgressBar>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default Nutrition;