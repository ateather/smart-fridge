import React, { Component } from 'react';
import { Container, Row, Col, Button, Carousel, Modal } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import '../App.css';
import recipes from '../constants/recipes.json';

class Recipes extends Component {
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.openRecipe = this.openRecipe.bind(this);
    this.InputStorage = []

    this.state = {
      "recipes" : recipes,
      show : false
      
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleSave() {
    let r = this.state.recipes
    r[this.InputStorage["AddName"]] = {
      name : this.InputStorage["AddName"],
      src :  this.InputStorage["AddSrc"]
    }

    this.setState({recipes : r, show : false})
  }

  search(query){

  }

  openRecipe(recipeName) {
    this.setState({redirect: true})
    this.props.history.push('/fridge/recipes/' + recipeName);
  }

  render() {
    let r = this.state.recipes
    let CarouselItems = []

    for (let key in r) {
      let recipe = r[key]

      CarouselItems.push(
        <Carousel.Item key={recipe.name} onClick={()=> this.openRecipe(recipe.name)}>
          <img
            className="d-block w-100"
            height={600}
            src={recipe.src}
            alt={recipe.name}
          />
          <Carousel.Caption>
            <h1>{recipe.name}</h1>
          </Carousel.Caption>
        </Carousel.Item>
      )
    }

    return (
      <Container className="App">
        <Row style={{paddingLeft:'15px', paddingBottom:'10px'}}>
          <Col sm={8}>
            <input type="text" style={{"color" : "white"}} className="form-control bg-primary" placeholder="Search"/>
          </Col>
          <Col xs>
            <input type="submit" className='btn btn-primary'></input>
          </Col>
          <Col xs>
            <Button className='btn btn-primary' onClick={this.handleShow}>Add New Recipe</Button>
          </Col>
        </Row>
        
        <Carousel>
          {CarouselItems}
        </Carousel>

        <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Add New Recipe</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Row>
                <Col>
                  <input type="text" style={{"color" : "white"}} className="form-control bg-primary" 
                    placeholder="Beans" 
                    value={this.InputStorage["AddName"]} 
                    onChange={(e) => {this.handleChange.call(this, e, "AddName")}}/>
                </Col>
              </Row>
              <Row>
                <Col>
                  <input type="text" style={{"color" : "white"}} className="form-control bg-primary" 
                    placeholder="http://reallycoolhost.com/image.jpeg" 
                    value={this.InputStorage["AddSrc"]} 
                    onChange={(e) => {this.handleChange.call(this, e, "AddSrc")}}/>
                </Col>
              </Row>
              <Row>
                <Col>
                  <textarea className="navbar-dark bg-primary" style={{marginTop: '10px', height: '200px', width : '100%', color: 'white'}} 
                    rows="10" 
                    placeholder="Cooking Instructions" 
                    onChange={(e) => {this.handleChange.call(this, e, "Notes")}} 
                    value={this.InputStorage["Notes"]}></textarea>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={this.handleSave.bind(this)}>
                Save Recipe
              </Button>
            </Modal.Footer>
          </Modal>

      </Container>
    );
  }
}

export default Recipes;