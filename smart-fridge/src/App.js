import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import Mobile_Wrapper  from "./mobile/Wrapper.js"
import Fridge_Wrapper from "./fridge/Wrapper.js"

import ButterChicken from "./fridge/ButterChicken.js"
import AngelChickenPasta from "./fridge/AngelChickenPasta.js"

import Landing from "./landing.js"
import './App.css';
import './bootstrap.css';
import ChickenCurry from './fridge/ChickenCurry.js';


class App extends Component {
  render() {
    return (
      <Router>
          <Route exact path="/" component={Landing} />

          <Route path="/mobile" component={Mobile_Wrapper} />
          <Route path="/fridge" component={Fridge_Wrapper} />
          <Route path="/fridge/recipes/Butter Chicken" component={ButterChicken} />
          <Route path="/fridge/recipes/Angel Chicken Pasta" component={AngelChickenPasta} />
          <Route path="/fridge/recipes/Chicken Curry" component={ChickenCurry} />             

      </Router>
    );
  }
}

export default App;
