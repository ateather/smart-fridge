import React, { Component } from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import logo from './logo.svg';
import './App.css';

class Landing extends Component {
  render() {
    return (
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>
                Edit <code>src/landing.js</code> and save to reload.
            </p>
            <Link to="/mobile">Go to Mobile Homescreen</Link>
            <Link to="/fridge">Go to Fridge Homescreen</Link>
        </header>
    );
  }
}

export default Landing;


