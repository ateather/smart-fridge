import React, { Component } from 'react';
import { Container} from 'react-bootstrap';
import { Route } from "react-router-dom";
import Navbar from './Navbar'
import '../App.css';

import HomeScreen from "./HomeScreen.js"
import Inventory from "./Inventory.js"
import ShoppingList from "./ShoppingList.js"
import LiveView from "./LiveView.js"

class Wrapper extends Component {
  render() {
    return (
        <div>
          <Navbar/>
          <Container className="App" style={{'marginTop':'40px'}}>
            
            <Route exact path="/mobile" component={ShoppingList} />
            <Route exact path="/mobile/inventory" component={Inventory} />
            <Route exact path="/mobile/liveView" component={LiveView} />
            <Route exact path="/mobile/list" component={ShoppingList} />

          </Container>
        </div>
    );
  }
}

export default Wrapper;
