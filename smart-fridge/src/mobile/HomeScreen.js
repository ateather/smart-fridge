import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import NotificationButton from "../components/NotifButton.js" 

class HomeScreen extends Component {
  render() {
    return (
      <div>
        <div style={{'marginTop':'12%'}}>
          <Row>
              <Col>
                  <h1>Your Fridge</h1>
              </Col>
          </Row>
          <Row>
              <Col>
                  <Button as={Link} to="/mobile/inventory" variant="primary" size="lg" block>Inventory</Button>
                  <Button as={Link} to="/mobile/list" variant="primary" size="lg" block>Shopping List</Button>
                  <Button as={Link} to="/mobile/liveView" variant="secondary" size="lg" block>Live View</Button>
              </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default HomeScreen;
