import React from 'react';
import { Nav, Button } from 'react-bootstrap';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

class Navbar extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      current : "list"
    }
  }


  render() {
    let active = {fontSize : "18px", "backgroundColor" : "gray"}
    let inactive = {fontSize : "18px"}
    var cur = window.location.pathname;


    return (
      <Nav fill variant="tabs" defaultActiveKey="/list">
        <Nav.Item>
            <Nav.Link style={(cur=="/mobile/inventory") ? active : inactive} as={Link} to="/mobile/inventory" eventKey="ree">
              <b>Inventory</b>
              
            </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link style={(cur=="/mobile/LiveView") ? active : inactive} as={Link} to="/mobile/LiveView" eventKey="link-1">
            <b>Live View</b>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link style={(cur=="/mobile/list" || cur=="/mobile") ? active : inactive} as={Link} to="/mobile/list" eventKey="link-2">
            <b>Shopping List</b>
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link eventKey="disabled" as={Button} block>
            Kevin
          </Nav.Link>
        </Nav.Item>
      </Nav>
    );
  }
}

export default Navbar;
