import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import img from "../res/FridgeLiveView.jpg";
import '../App.css';

class LiveView extends Component {
  render() {
    return (
      <Container className="App">
        <Row style={{paddingTop:"25px"}}>
            <Col>
                <img style={{height:'20%'}} src={img}/>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default LiveView;