import React, { Component } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import '../App.css';
import inventory from "../constants/inventory.json"

const FSIZE = "20px"

class Recipes extends Component {

  constructor(props) {
    super(props)
    this.InputStorage = []
    this.state = {
      "inventory" : inventory,
      "adding" : false
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  doneAdding() {
    let done = !this.state.adding
    this.setState({adding : done})
  }

  addNewItem() {
    let AddCount = this.InputStorage["AddCount"] || 1
    if (this.InputStorage["AddName"] && AddCount > 0 && this.InputStorage['AddDate']) {
      let nInventory = this.state.inventory
      nInventory[this.InputStorage["AddName"]] = {
        name : this.InputStorage["AddName"],
        count : AddCount,
        expired : false,
        expiration : new Date(this.InputStorage['AddDate'])
      }

      /*this.InputStorage["AddName"] = null
      this.InputStorage["AddDate"] = null
      this.InputStorage["AddCount"] = null*/

      this.setState({ inventory : nInventory })
    }
  }

  updateItem(name, newValue) {
    let nInventory = this.state.inventory
    if (nInventory[name]) {
      nInventory[name].count = (newValue > 0) ? newValue : 0 
    }

    this.setState(state => ({
      "inventory" : nInventory
    }))
  }

  removeItem(name) {
    let nInventory = this.state.inventory
    if (nInventory[name]) {
      delete nInventory[name]
    }

    this.setState(state => ({
      "inventory" : nInventory
    }))
  }

  render() {
    let inv = this.state.inventory
    let RenderMe = []

    

    RenderMe.push(<> 
    <Row key="_xxx_" style={{'font-size':FSIZE}}>
      <Col>
        <span className={"badge align-middle"}>Item</span>
      </Col>
      <Col xs>
        <span className={"badge align-middle"} style={{"marginRight" : "20px", "textAlign" : "right"}}>Count</span>
      </Col>
      <Col xs>
        <span className={"badge align-middle"}>Expiration</span>
      </Col>
      
    </Row>
    <br/>
    </>)
    for (let key in inv) {
      let i = inv[key]
      i.expiration = new Date(i.expiration)
      i.expired = (i.expiration <= new Date(Date.now())) 

      let dateOffset = (24*60*60*1000) * 4; 
      let warningDate = new Date(i.expiration);
      warningDate.setTime(warningDate.getTime() - dateOffset);
      let isClose = (warningDate <= new Date(Date.now()) )
      

      

      RenderMe.push((
        <Row key={i.name} style={{'font-size':FSIZE}}>
          <Col>
            <span className="align-middle">{i.name}</span>
          </Col>
          <Col xs>
            <span className={i.expired ? "badge" : "badge"} style={{"marginRight" : "20px", "textAlign" : "right"}}>{i.count}</span>
          </Col>
          <Col xs>
            <span className={i.expired ? "badge badge-danger" : (isClose) ? "badge badge-warning" : "badge"}>{(i.expiration.getMonth()+1) + "/" + i.expiration.getDate()}</span>
          </Col>
          
        </Row>
      ))
    }
    
    let header = (<div></div>)
    if (!this.state.adding) {
      header = (
        <Row>
          <Col>
            <h1>Inventory</h1>
          </Col>
          
        </Row>
      )
    } else {
      
      header = (
      <Row>
        <Col sm={4}>
          <input type="text" style={{"color" : "white"}} className="form-control bg-primary" placeholder="Beans" value={this.InputStorage["AddName"]} onChange={(e) => {this.handleChange.call(this, e, "AddName")}}/>
        </Col>

        <Col xs>
          <input type="text" style={{"textAlign" : "right", "color" : "white"}} className="form-control bg-primary" pattern="[0-9]*" value={this.InputStorage["AddCount"] || 1} onChange={(e) => {this.handleChange.call(this, e, "AddCount")}}/>
        </Col>

        <Col sm={2}>  
          <input type="text" style={{"color" : "white"}} className="form-control bg-primary" placeholder="05/20/2019" pattern="[0-1][0-9]/[0-3][0-9]/[0-2][0-9][0-9][0-9]" onChange={(e) => {this.handleChange.call(this, e, "AddDate")}}/>
        </Col>

        <Col xs>
          <Button variant="outline-light" id="button-addon2" block onClick={this.addNewItem.bind(this)} >Add</Button>  
        </Col>
        <Col xs>
          <Button variant="outline-secondary" id="button-addon2" block onClick={this.doneAdding.bind(this)}>Done</Button>
        </Col>
            
        
      </Row>
      )
    }

    return (
      <div>
        {header}
        <br/>
        {RenderMe}
      </div>
    );
  }
}

export default Recipes;