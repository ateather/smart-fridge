import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import '../App.css';
import shoppingList from "../constants/shoppinglist.json"

const FSIZE = "20px"

class ShoppingList extends Component {

  constructor(props) {
    super(props)
    this.InputStorage = []
    this.state = {
      "shoppingList" : shoppingList,
      "adding" : false
    }
  }

  handleChange(event, name) {
    this.InputStorage[name] = event.target.value
    this.setState({})
  }

  doneAdding() {
    let done = !this.state.adding
    this.setState({adding : done})
  }

  addNewItem() {
    let AddCount = this.InputStorage["AddCount"] || 1
    let Unit = this.InputStorage["Unit"] || ""
    if (this.InputStorage["AddName"] && AddCount > 0) {
      let nShoppingList = this.state.shoppingList
      nShoppingList[this.InputStorage["AddName"]] = {
        name : this.InputStorage["AddName"],
        count : AddCount,
        unit : Unit,
        expired : false
      }

      this.setState({ shoppingList : nShoppingList })
    }
  }

  updateItem(name, newValue) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      nShoppingList[name].count = (newValue > 0) ? newValue : 0
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  removeItem(name) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      delete nShoppingList[name]
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  check(name) {
    let nShoppingList = this.state.shoppingList
    if (nShoppingList[name]) {
      nShoppingList[name].checked = !nShoppingList[name].checked
    }

    this.setState(state => ({
      "shoppingList" : nShoppingList
    }))
  }

  render() {
    let sL = this.state.shoppingList
    let RenderMe = []
    for (let key in sL) {
      let i = sL[key]
      if (i.checked) {
        RenderMe.push((
          <Row key={i.name}>
            <Col xs={3} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
              <span>{i.name}</span>
            </Col>
            <Col xs style={{'text-align':'right', 'font-size':FSIZE}}>
              <span className={i.expired ? "badge badge-warning" : "badge"} style={{ "textAlign" : "right"}}>{i.count}</span>
            </Col>
            <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
              <span>{i.unit}</span>
            </Col>
            <Col xs>
              <Button variant="outline-danger" onClick={this.removeItem.bind(this, i.name)}>x</Button>
            </Col>
            <Col xs>
              <Button active variant="outline-success float-right" onClick={this.check.bind(this, i.name)}>&#10003;</Button>
            </Col>
          </Row>
        ))
      } else {
        RenderMe.push((
          <Row key={i.name}>
            <Col xs={3} style={{'text-align':'left', 'font-size':FSIZE, 'overflow':'hidden', 'white-space':'nowrap'}}>
              <span>{i.name}</span>
            </Col>
            <Col xs style={{'text-align':'right', 'font-size':FSIZE}}>
              <span className={i.expired ? "badge badge-warning" : "badge"} style={{ "textAlign" : "right"}}>{i.count}</span>
            </Col>
            <Col xs style={{'text-align':'left', 'font-size':FSIZE}}>
              <span>{i.unit}</span>
            </Col>
            <Col xs>
              <Button variant="outline-danger" onClick={this.removeItem.bind(this, i.name)}>x</Button>
            </Col>
            <Col xs>
              <Button variant="outline-success float-right" onClick={this.check.bind(this, i.name)}>&#10003;</Button>
            </Col>
          </Row>
        ))
      }
      /*RenderMe.push((
        <div style={{'border-bottom':'1px solid white'}}></div>
      ))*/
    }

    let header = (<h1>Shopping List</h1>)

    let footer = (<div></div>)
    if (!this.state.adding) {
      footer = (
        <Row>
          <Col>
            <Button style={{'font-size':FSIZE}} variant="secondary" onClick={this.doneAdding.bind(this)}>Add Item</Button>
          </Col>
        </Row>
      )
    } else {
      

      footer = (
      <Row>
        <Col sm={7}>
          <input type="text" style={{"color" : "white"}} className="form-control bg-primary" placeholder="Item's name" onChange={(e) => {this.handleChange.call(this, e, "AddName")}}/>
        </Col>

        <Col xs>
          <input type="text" style={{"textAlign" : "right", "color" : "white"}} className="form-control bg-primary" pattern="[0-9]*" value={this.InputStorage["AddCount"] || 1} onChange={(e) => {this.handleChange.call(this, e, "AddCount")}}/>
        </Col>

        <Col xs>
          <input type="text" style={{"textAlign" : "right", "color" : "white"}} className="form-control bg-primary"  placeholder='Unit' value={this.InputStorage["Unit"] || ""} onChange={(e) => {this.handleChange.call(this, e, "Unit")}}/>
        </Col>

        <Col xs>
          <Button variant="outline-light" id="button-addon2" block onClick={this.addNewItem.bind(this)} >Add</Button>
        </Col>
        <Col xs>
          <Button variant="outline-secondary" id="button-addon2" block onClick={this.doneAdding.bind(this)}>Done</Button>
        </Col>


      </Row>
      )
    }

    return (
      <div>
        {header}
        <br/>
        {RenderMe}
        <br/>
        {footer}
        <br/>
      </div>
    );
  }

}

export default ShoppingList;