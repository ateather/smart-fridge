## Installing 

Install Node.js if you haven't already, then run:

```
npm install
cd smart-fridge
npm install
```

## Running

Running this app will start a local server to relay updates on. Restarting the server is not needed as you will see updates to your code live as your browser refershes. However, this means the app requires a network connection. 

Run as follows:

```
cd smart-fridge
npm start
```


## Development

Checkout and use the `dev` branch. So when pushing a commit use:

```
git push origin dev
```


For the most part we will be working in the `/src` directoy on `.js` files. 

Keep centralized styling in `/src/App.css`
